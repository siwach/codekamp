AccountsTemplates.configureRoute('signUp', {
    name: 'signUp',
    path: '/xyz',
    template: 'RegisterNow',
    redirect: '/'
});
AccountsTemplates.configureRoute('signIn', {
    name: 'signIn',
    path: '/abc',
    template: 'SignIn',
    redirect: '/'
});
//
//AccountsTemplates.configureRoute('SignIn', {
//    redirect: function(){
//        var user = Meteor.user();
//        if (user)
//            Router.go('/user/' + user._id);
//    }
//});
//AccountsTemplates.configureRoute('RegisterNow', {
//    redirect: function(){
//        var user = Meteor.user();
//        if (user)
//            Router.go('/user/' + user._id);
//    }
//});



AccountsTemplates.addField({
    _id: 'EnrollmentNumber',
    type: 'text',
    required: true,
    displayName: "Codekamp Enrollment Number"
});
