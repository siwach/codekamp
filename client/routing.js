Meteor.startup(function () {
        $('body').scrollspy({target: '#bs-example-navbar-collapse-1'});
    }
);

var userLoggingIn = function(){
    if(Meteor.loggingIn()){
        this.render('loading');
    } else {
        this.next();
    }
}

// code for routing

Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading'
});

Router.route('/', {name: 'home'});
Router.route('/profile',{name: 'profile'});
Router.route('/profile/editprofile',{name: 'editProfile'});

Router.route('/logout', {name: 'logout', action: function () {
    Meteor.logout();
    this.redirect('/');
}});

Router.onBeforeAction(userLoggingIn);



